import path from "path";

/**
 * @type {import("vite").UserConfig}
 */
const config = {
  resolve: {
    alias: {
      "#~": path.resolve(import.meta.dirname, "./src/"),
    },
  },
};

export default config;
