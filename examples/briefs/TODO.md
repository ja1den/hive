# TODO

## Introduction

TODO is an application that allows users to manage a list of tasks.

It has a minimal, modern interface designed to reduce clutter and improve focus.

## Layout

Each page of the application, aside from the login page, has a navigation bar at the top.

The navigation bar shows the name of the application, TODO, and the user's current authentication status.

If the user is logged in, the navigation bar shows their name.
If the user is not logged in, the navigation bar shows a "Log In" button.

## Pages

### `/`

The index page allows the user to view and manage their list of tasks.

For each task in the list, the user can perform several operations:

- Toggle whether the task is marked as complete.
- Edit the description of the task.
- Delete the task.

The user can also create new tasks.

If the user is not logged in, no tasks should be shown, and the user should instead see a link to the login page (`/login`).

### `/login`

The login page allows the user to enter their email and password to log in.
