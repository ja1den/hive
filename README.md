<div align="center">
  <p>
    <img src="./media/icon.svg" alt="Hive" width="128">
  </p>

  <h1>Hive</h1>

  <p>
    An LLM agent capable of acting as a honeypot.
  </p>
</div>

## Introduction

Hive is an LLM agent capable of acting as a honeypot.

It's built with [Node.js](https://nodejs.org/en) and [Ollama](https://ollama.com/).

## Usage

### CLI

Hive exposes a CLI with several commands:

```sh
❯ hive --help
Usage: hive [options] [command]

Options:
  -c, --config <config>  Configuration file to read from
  -h, --help             Display help for a command

Commands:
  check                  Check whether the configured model is available
  build                  Build the route table based on the brief
  serve                  Serve the application from the route table
  reset <table>          Reset a database table
```

### Configuration

The configuration file exposes several additional options:

| Path    | Type     | Required | Description                                          | Example                      |
| ------- | -------- | -------- | ---------------------------------------------------- | ---------------------------- |
| `brief` | `string` | `true`   | Path to a brief, relative to the configuration file. | `./briefs/TODO.md`           |
| `model` | `string` | `true`   | Model to use for generation.                         | `codellama:7b-instruct-q4_0` |

For an example configuration, see [`examples/`](./examples/).

### Ollama

To connect to Ollama, Hive reads the `OLLAMA_HOST` environment variable on startup.

If `OLLAMA_HOST` is not specified, it defaults to `http://127.0.0.1:11434`.

### Database

Hive stores data in `sqlite.db` in the current working directory.

If `sqlite.db` does not exist, create it and the necessary tables with the [`db:push`](#dbpush) script.

## Contribution

Hive is built with:

- [Node.js](https://nodejs.org/)
- [pnpm](https://pnpm.io/)
- [Ollama](https://ollama.com/)

This repository provides a [devenv](https://devenv.sh/) containing the supported versions of these packages.

To activate the devenv, install [Nix](https://nixos.org/) and run:

```sh
nix develop --impure
```

To activate the devenv automatically when entering the directory, install [direnv](https://direnv.net/) and grant permission to load the [`.envrc`](./.envrc):

```sh
direnv allow
```

### Dependencies

Install Hive's dependencies with `pnpm`:

```sh
pnpm install
```

### Ollama

To start Ollama with `devenv`, run:

```sh
devenv up
```

To start it manually, run:

```sh
ollama serve
```

### Scripts

#### `build`

Build Hive, placing the result in `dist/`.

This script is broken into several parts, which may be run individually:

- `build:copy` - Copy the [Nunjucks](https://mozilla.github.io/nunjucks/) templates to `dist/`.
- `build:typescript` - Compile the [TypeScript](https://typescriptlang.org/).

#### `db:generate`

Generate a database migration with [Drizzle Kit](https://orm.drizzle.team/kit-docs).

#### `db:push`

Push the current [schema](./src/schema.ts) to the database with Drizzle Kit.

> This action is potentially destructive.

#### `dev`

Start Hive with [`nodemon`](https://nodemon.io/), which will trigger a restart if changes are detected.

In development, it is recommended to couple this script with the [`watch`](#watch) script.

#### `format`

Format the repository.

This script is broken into several parts, which may be run individually:

- `format:eslint` - Fix problems detected by [ESLint](https://eslint.org/).
- `format:prettier` - Fix problems detected by [Prettier](https://prettier.io/).
- `format:sort` - Sort `package.json` and other configuration files.

#### `lint`

Lint the repository.

This script is broken into several parts, which may be run individually:

- `format:eslint` - Find problems with ESLint.
- `format:prettier` - Find problems with Prettier.
- `format:typescript` - Find problems with TypeScript.

#### `start`

Start the compiled version of Hive in `dist/`.

#### `watch`

Start the build process in watch mode.

This script is broken into several parts, which may be run individually:

- `watch:copy` - Copy the Nunjucks templates to `dist/`.
- `watch:typescript` - Compile the TypeScript.
