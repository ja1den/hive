/**
 * @type {import("drizzle-kit").Config}
 */
const config = {
  dbCredentials: {
    url: "./sqlite.db",
  },
  dialect: "sqlite",
  migrations: {
    table: "migrations",
  },
  out: "./migrations",
  schema: "./src/schema.ts",
};

export default config;
