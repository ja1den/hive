CREATE TABLE `message` (
	`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL,
	`type` text NOT NULL,
	`route_id` text NOT NULL,
	`route_method` text NOT NULL,
	`content` text,
	`content_type` text,
	FOREIGN KEY (`type`) REFERENCES `message_type`(`id`) ON UPDATE no action ON DELETE no action,
	FOREIGN KEY (`route_id`,`route_method`) REFERENCES `route`(`id`,`method`) ON UPDATE no action ON DELETE no action
);
--> statement-breakpoint
CREATE TABLE `message_type` (
	`id` text PRIMARY KEY NOT NULL
);
