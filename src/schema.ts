import {
  foreignKey,
  integer,
  primaryKey,
  sqliteTable,
  text,
} from "drizzle-orm/sqlite-core";

export type SelectRouteType = typeof RouteType.$inferSelect;
export type InsertRouteType = typeof RouteType.$inferInsert;

export const RouteType = sqliteTable("route_type", {
  id: text("id", {
    enum: ["page", "internal"],
  }).primaryKey(),
});

export type SelectRouteMethod = typeof RouteMethod.$inferSelect;
export type InsertRouteMethod = typeof RouteMethod.$inferInsert;

export const RouteMethod = sqliteTable("route_method", {
  id: text("id", {
    enum: ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"],
  }).primaryKey(),
});

export type SelectRoute = typeof Route.$inferSelect;
export type InsertRoute = typeof Route.$inferInsert;

export const Route = sqliteTable(
  "route",
  {
    id: text("id").notNull(),
    method: text("method", {
      enum: RouteMethod.id.enumValues,
    })
      .notNull()
      .references(() => RouteMethod.id),
    type: text("type", {
      enum: RouteType.id.enumValues,
    })
      .notNull()
      .references(() => RouteType.id),
    // eslint-disable-next-line perfectionist/sort-objects
    name: text("name"),
  },
  (table) => ({
    pk: primaryKey({
      columns: [table.id, table.method],
    }),
  }),
);

export type SelectMessageType = typeof MessageType.$inferSelect;
export type InsertMessageType = typeof MessageType.$inferInsert;

export const MessageType = sqliteTable("message_type", {
  id: text("id", {
    enum: ["request", "reply"],
  }).primaryKey(),
});

export type SelectMessage = typeof Message.$inferSelect;
export type InsertMessage = typeof Message.$inferInsert;

export const Message = sqliteTable(
  "message",
  {
    id: integer("id", {
      mode: "number",
    }).primaryKey({
      autoIncrement: true,
    }),
    type: text("type", {
      enum: MessageType.id.enumValues,
    })
      .notNull()
      .references(() => MessageType.id),
    // eslint-disable-next-line perfectionist/sort-objects
    routeId: text("route_id").notNull(),
    routeMethod: text("route_method", {
      enum: Route.method.enumValues,
    }).notNull(),
    // eslint-disable-next-line perfectionist/sort-objects
    content: text("content"),
  },
  (table) => ({
    fkRoute: foreignKey({
      columns: [table.routeId, table.routeMethod],
      foreignColumns: [Route.id, Route.method],
    }),
  }),
);

export default {
  Message,
  MessageType,
  Route,
  RouteMethod,
  RouteType,
};
