import process from "process";

import chalk from "chalk";
import { Command } from "commander";
import { sql } from "drizzle-orm";

import db from "#~/lib/db.js";

import { default as schema } from "#~/schema.js";

const execute = async function (this: Command, table: string) {
  await db.transaction(async (tx) => {
    switch (table) {
      case "route":
        await tx.delete(schema.Route);
        break;

      case "message":
        await tx.delete(schema.Message);
        break;

      default:
        this.error(`Error: Table '${table}' not found`, {
          code: "hive.reset.table",
        });
    }

    tx.run(sql`
      DELETE
      FROM
        "sqlite_sequence"
      WHERE
        "name" = ${table}
      `);
  });

  process.stdout.write(`${chalk.green(table)}\n`);
};

const command = new Command();

command
  .name("reset")
  .description("Reset a database table")
  .argument("<table>", "The table to reset")
  .action(execute);

export default command;
