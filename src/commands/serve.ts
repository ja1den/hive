import fs from "fs";
import path from "path";

import closeWithGrace from "close-with-grace";
import { Command } from "commander";
import Fastify from "fastify";

import formbody from "@fastify/formbody";
import sensible from "@fastify/sensible";

import routes from "#~/routes/index.js";

import type { Opts } from "#~/index.js";

const execute = async function (this: Command) {
  const opts = this.optsWithGlobals<Opts>();

  const briefPath = path.join(opts.configPath, "..", opts.config.brief);

  let brief;
  try {
    brief = fs.readFileSync(briefPath, { encoding: "utf-8" });
  } catch (e) {
    if (e instanceof Error)
      this.error(`Error: ${e.message}`, { code: "hive.build.readBrief" });

    throw e;
  }

  const fastify = Fastify({ logger: true });

  fastify.register(formbody);
  fastify.register(sensible);

  fastify.register(routes, { brief, opts });

  const closeListeners = closeWithGrace({ delay: 500 }, async ({ err }) => {
    if (err) fastify.log.error(err);

    await fastify.close();
  });

  try {
    fastify.addHook("onClose", closeListeners.uninstall);

    await fastify.listen({ port: 3000 });
  } catch (e) {
    if (e instanceof Error)
      this.error(`Error: ${e.message}`, { code: "hive.serve.listen" });

    throw e;
  }
};

const command = new Command();

command
  .name("serve")
  .description("Serve the application from the route table")
  .action(execute);

export default command;
