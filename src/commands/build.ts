import fs from "fs";
import path from "path";

import chalk from "chalk";
import { Command } from "commander";
import { createInsertSchema } from "drizzle-zod";
import ora from "ora";
import { ZodError, z as zod } from "zod";
import { fromZodError } from "zod-validation-error";

import db from "#~/lib/db.js";
import nunjucks from "#~/lib/nunjucks.js";
import ollama from "#~/lib/ollama.js";

import { default as schema } from "#~/schema.js";

import type { Message } from "ollama";

import type { Opts } from "#~/index.js";
import type { InsertRoute } from "#~/schema.js";

const Page = zod.object({
  routes: zod.array(
    createInsertSchema(schema.Route)
      .pick({
        id: true,
        name: true,
      })
      .required({
        name: true,
      }),
  ),
});

const Internal = zod.object({
  routes: zod.array(
    createInsertSchema(schema.Route).pick({
      id: true,
      method: true,
    }),
  ),
});

const execute = async function (this: Command) {
  const opts = this.optsWithGlobals<Opts>();

  const briefPath = path.join(opts.configPath, "..", opts.config.brief);

  let brief;
  try {
    brief = fs.readFileSync(briefPath, { encoding: "utf-8" });
  } catch (e) {
    if (e instanceof Error)
      this.error(`Error: ${e.message}`, { code: "hive.build.readBrief" });

    throw e;
  }

  const routes = {
    page: [] as InsertRoute[],
    // eslint-disable-next-line perfectionist/sort-objects
    internal: [] as InsertRoute[],
  };

  const messages: Message[] = [];

  messages.push({
    content: nunjucks.render("build/brief.njk", {
      brief,
    }),
    role: "system",
  });

  const spinner = ora();

  spinner.text = "page";
  spinner.start();

  {
    messages.push({
      content: nunjucks.render("build/plan/page.njk"),
      role: "user",
    });

    const response = await ollama.chat({
      messages,
      model: opts.config.model,
      options: {
        seed: 0,
        temperature: 0,
      },
    });

    messages.push(response.message);
  }

  {
    messages.push({
      content: nunjucks.render("build/json/page.njk"),
      role: "user",
    });

    const response = await ollama.chat({
      format: "json",
      messages,
      model: opts.config.model,
      options: {
        seed: 0,
        temperature: 0,
      },
    });

    messages.push(response.message);

    try {
      Page.parse(JSON.parse(response.message.content))
        .routes.map<InsertRoute>((route) => ({
          ...route,
          method: "GET",
          type: "page",
        }))
        .forEach((route) => routes.page.push(route));
    } catch (e) {
      if (e instanceof ZodError)
        this.error(
          `Error: Failed to parse response: ${fromZodError(e, { prefix: null }).message}`,
          {
            code: "hive.build.parseResponse",
          },
        );

      throw e;
    }
  }

  spinner.succeed();

  let i = 1;

  for (const page of routes.page) {
    await (async (messages) => {
      spinner.text = [
        "internal",
        chalk.grey(`(${i.toString()}/${routes.page.length.toString()})`),
        chalk.cyan(page.id),
      ].join(" ");
      spinner.start();

      {
        messages.push({
          content: nunjucks.render("build/plan/internal.njk", {
            page,
          }),
          role: "user",
        });

        const response = await ollama.chat({
          messages,
          model: opts.config.model,
          options: {
            seed: 0,
            temperature: 0,
          },
        });

        messages.push(response.message);
      }

      {
        messages.push({
          content: nunjucks.render("build/json/internal.njk", {
            page,
          }),
          role: "user",
        });

        const response = await ollama.chat({
          format: "json",
          messages,
          model: opts.config.model,
          options: {
            seed: 0,
            temperature: 0,
          },
        });

        messages.push(response.message);

        try {
          Internal.parse(JSON.parse(response.message.content))
            .routes.map<InsertRoute>((route) => ({
              ...route,
              type: "internal",
            }))
            .forEach((route) => routes.internal.push(route));
        } catch (e) {
          if (e instanceof ZodError)
            this.error(
              `Error: Failed to parse response: ${fromZodError(e, { prefix: null }).message}`,
              {
                code: "hive.build.parseResponse",
              },
            );

          throw e;
        }
      }

      spinner.succeed();

      i++;
    })([...messages]);
  }

  await db.transaction(async (tx) => {
    await tx.delete(schema.Route);
    await tx.insert(schema.Route).values(routes.page);
    await tx.insert(schema.Route).values(routes.internal);
  });
};

const command = new Command();

command
  .name("build")
  .description("Build the route table based on the brief")
  .action(execute);

export default command;
