import process from "process";

import chalk from "chalk";
import { Command } from "commander";

import type { Opts } from "#~/index.js";

// eslint-disable-next-line @typescript-eslint/require-await
const execute = async function (this: Command) {
  const opts = this.optsWithGlobals<Opts>();

  process.stdout.write(`${chalk.green(opts.config.model)}\n`);
};

const command = new Command();

command
  .name("check")
  .description("Check whether the configured model is available")
  .action(execute);

export default command;
