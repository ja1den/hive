import chalk from "chalk";
import { Command, InvalidArgumentError } from "commander";

import { readFromJSON } from "#~/lib/config.js";
import { checkModel } from "#~/lib/ollama.js";

import build from "#~/commands/build.js";
import check from "#~/commands/check.js";
import reset from "#~/commands/reset.js";
import serve from "#~/commands/serve.js";

import type { Config } from "#~/lib/config.js";

export interface Opts {
  config: Config;
  configPath: string;
}

const program = new Command();

program
  .name("hive")
  .requiredOption(
    "-c, --config <config>",
    "Configuration file to read from",
    (configPath) => {
      let config;
      try {
        config = readFromJSON(configPath);
      } catch (e) {
        if (e instanceof Error) throw new InvalidArgumentError(e.message);

        throw e;
      }

      program.setOptionValue("configPath", configPath);

      return config;
    },
  )
  .helpOption("-h, --help", "Display help for a command")
  .helpCommand(false)
  .configureHelp({
    showGlobalOptions: true,
  });

program.hook("preAction", async (command) => {
  const opts = command.opts<Opts>();

  try {
    await checkModel(opts.config.model);
  } catch (e) {
    if (e instanceof Error)
      program.error(`Error: ${e.message}`, { code: "hive.checkModel" });

    throw e;
  }
});

for (const command of [check, build, serve, reset]) {
  command.copyInheritedSettings(program);
  program.addCommand(command);
}

program
  .configureOutput({
    outputError: (str, write) => {
      write(
        chalk.red(
          str.replace(
            /^Error:(\s*)([a-z])?/i,
            (_match, ...groups: string[]) =>
              `Error:${groups[0] ?? ""}${groups[1]?.toUpperCase() ?? ""}`,
          ),
        ),
      );
    },
  })
  .showSuggestionAfterError(false);

await program.parseAsync();
