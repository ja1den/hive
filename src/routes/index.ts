import { ZodError, z as zod } from "zod";
import { fromZodError } from "zod-validation-error";

import db from "#~/lib/db.js";
import nunjucks from "#~/lib/nunjucks.js";
import ollama from "#~/lib/ollama.js";

import { default as schema } from "#~/schema.js";

import type { FastifyPluginAsync } from "fastify";
import type { Message } from "ollama";

import type { Opts } from "#~/index.js";

interface Options {
  brief: string;
  opts: Opts;
}

const Page = zod.object({
  body: zod.string(),
});

const plugin: FastifyPluginAsync<Options> = async (
  fastify,
  { brief, opts },
) => {
  const routes = await db.select().from(schema.Route);

  routes.forEach((route) =>
    fastify.route({
      handler: async (request, reply) => {
        let content: string;

        const messages: Message[] = [];

        messages.push({
          content: nunjucks.render("serve/brief.njk", {
            brief,
          }),
          role: "system",
        });

        messages.push({
          content: nunjucks.render("serve/routes.njk", {
            routes,
          }),
          role: "system",
        });

        messages.push({
          content: nunjucks.render("serve/messages.njk", {
            messages: await db.select().from(schema.Message),
          }),
          role: "system",
        });

        switch (route.type) {
          case "page": {
            {
              messages.push({
                content: nunjucks.render("serve/plan/page.njk", { route }),
                role: "user",
              });

              const response = await ollama.chat({
                messages,
                model: opts.config.model,
                options: {
                  seed: 0,
                  temperature: 0,
                },
              });

              messages.push(response.message);
            }

            {
              messages.push({
                content: nunjucks.render("serve/json/page.njk", { route }),
                role: "user",
              });

              const response = await ollama.chat({
                format: "json",
                messages,
                model: opts.config.model,
                options: {
                  seed: 0,
                  temperature: 0,
                },
              });

              messages.push(response.message);

              try {
                const { body } = Page.parse(
                  JSON.parse(response.message.content.replace(/ {2}|\n/g, "")),
                );

                content = nunjucks.render("serve/html/page.njk", {
                  body,
                  route,
                });
              } catch (e) {
                if (e instanceof ZodError) {
                  reply.internalServerError(
                    `Failed to parse response: ${fromZodError(e, { prefix: null }).message}`,
                  );
                  return;
                }

                throw e;
              }
            }

            reply.type("text/html");

            break;
          }

          case "internal":
            {
              messages.push({
                content: nunjucks.render("serve/plan/internal.njk", {
                  request,
                  route,
                }),
                role: "user",
              });

              const response = await ollama.chat({
                messages,
                model: opts.config.model,
                options: {
                  seed: 0,
                  temperature: 0,
                },
              });

              messages.push(response.message);
            }

            {
              messages.push({
                content: nunjucks.render("serve/json/internal.njk"),
                role: "user",
              });

              const response = await ollama.chat({
                format: "json",
                messages,
                model: opts.config.model,
                options: {
                  seed: 0,
                  temperature: 0,
                },
              });

              messages.push(response.message);

              content = response.message.content;
            }

            break;
        }

        await db.insert(schema.Message).values([
          {
            type: "request",
            // eslint-disable-next-line perfectionist/sort-objects
            routeId: route.id,
            routeMethod: route.method,
            // eslint-disable-next-line perfectionist/sort-objects
            content:
              request.headers["content-type"] === "application/json"
                ? JSON.stringify(request.body, null, 2)
                : (request.body as string | undefined) ?? null,
          },
          {
            type: "reply",
            // eslint-disable-next-line perfectionist/sort-objects
            routeId: route.id,
            routeMethod: route.method,
            // eslint-disable-next-line perfectionist/sort-objects
            content,
          },
        ]);

        return content;
      },
      method: route.method,
      url: route.id,
    }),
  );
};

export default plugin;
