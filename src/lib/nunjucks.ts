import path from "path";

import { Environment, FileSystemLoader } from "nunjucks";

const loader = new FileSystemLoader(
  path.resolve(import.meta.dirname, "..", "templates"),
);
const environment = new Environment(loader, {
  autoescape: false,
  lstripBlocks: true,
  trimBlocks: true,
});

export default environment;
