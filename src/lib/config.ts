import { readFileSync } from "fs";
import { resolve } from "path";

import { ZodError, z as zod } from "zod";
import { fromZodError } from "zod-validation-error";

export type Config = zod.infer<typeof Config>;

export const Config = zod
  .object({
    brief: zod.string(),
    model: zod.string(),
  })
  .strict();

/**
 * Read a {@link Config} object from a JSON file.
 *
 * @param path The path to the configuration file to read from.
 * @returns The {@link Config} object.
 */
export const readFromJSON = (path: string): Config => {
  const configPath = resolve(path);
  const configString = readFileSync(configPath, { encoding: "utf-8" });
  const configData: unknown = JSON.parse(configString);

  let config;
  try {
    config = Config.parse(configData);
  } catch (e) {
    if (e instanceof ZodError) throw fromZodError(e, { prefix: null });
    else throw e;
  }

  return config;
};
