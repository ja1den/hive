import Database from "better-sqlite3";
import { drizzle } from "drizzle-orm/better-sqlite3";

import { default as schema } from "#~/schema.js";

const sqlite = new Database("sqlite.db");
const db = drizzle(sqlite, { schema });

await db
  .insert(schema.RouteType)
  .values(schema.RouteType.id.enumValues.map((id) => ({ id })))
  .onConflictDoNothing();

await db
  .insert(schema.RouteMethod)
  .values(schema.RouteMethod.id.enumValues.map((id) => ({ id })))
  .onConflictDoNothing();

await db
  .insert(schema.MessageType)
  .values(schema.MessageType.id.enumValues.map((id) => ({ id })))
  .onConflictDoNothing();

export default db;
