import { Ollama } from "ollama";

const OLLAMA_HOST = process.env["OLLAMA_HOST"];

const config = OLLAMA_HOST !== undefined ? { host: OLLAMA_HOST } : undefined;

const ollama = new Ollama(config);

export default ollama;

/**
 * Check the availability of a model.
 *
 * @param model The model to check for.
 * @return
 */
export const checkModel = async (model: string): Promise<void> => {
  try {
    await ollama.show({ model });
  } catch (e) {
    if (e instanceof Error) throw e.cause instanceof Error ? e.cause : e;
    else throw e;
  }
};
