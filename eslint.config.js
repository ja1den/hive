import js from "@eslint/js";
import globals from "globals";
import ts from "typescript-eslint";

import eslintPluginPerfectionistRecommendedNatural from "eslint-plugin-perfectionist/configs/recommended-natural";
import eslintPluginPrettierRecommended from "eslint-plugin-prettier/recommended";

/**
 * @type {import("eslint").Linter.FlatConfig[]}
 */
const config = [
  {
    ignores: ["dist/"],
  },
  {
    languageOptions: {
      globals: globals.node,
    },
  },
  js.configs.recommended,
  ...ts.configs.strictTypeChecked,
  ...ts.configs.stylisticTypeChecked,
  eslintPluginPrettierRecommended,
  eslintPluginPerfectionistRecommendedNatural,
  {
    languageOptions: {
      parserOptions: {
        project: true,
      },
    },
  },
  {
    files: ["**/*.js"],
    ...ts.configs.disableTypeChecked,
  },
  {
    rules: {
      "@typescript-eslint/no-floating-promises": ["off"],
      "@typescript-eslint/no-unused-vars": [
        "error",
        {
          args: "all",
          argsIgnorePattern: "^_",
          destructuredArrayIgnorePattern: "^_",
          ignoreRestSiblings: true,
          varsIgnorePattern: "^_",
        },
      ],
      "perfectionist/sort-imports": [
        "error",
        {
          "custom-groups": {
            value: {
              "external-eslint-plugin": [
                "eslint-plugin-*",
                "eslint-plugin-*/**",
              ],
              "external-fastify": "@fastify/**",
              "internal-lib": "#~/lib/**",
            },
          },
          groups: [
            "builtin",
            "external",
            "external-fastify",
            "external-eslint-plugin",
            "internal-lib",
            "internal",
            "index",
            ["parent", "sibling"],
            "side-effect",
            "object",
            "type",
            "builtin-type",
            "external-type",
            "internal-type",
            "index-type",
            ["parent-type", "sibling-type"],
            "side-effect-style",
            "style",
            "unknown",
          ],
          "internal-pattern": ["#~/**"],
        },
      ],
    },
  },
];

export default config;
