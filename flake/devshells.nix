{inputs, ...}: {
  perSystem = {pkgs, ...}: {
    devShells = {
      "default" = inputs.devenv.lib.mkShell {
        inherit inputs pkgs;

        modules = [
          {
            languages = {
              javascript = {
                enable = true;

                pnpm = {
                  enable = true;

                  install = {
                    enable = true;
                  };
                };
              };

              nix = {
                enable = true;
              };
            };

            packages = [
              pkgs.nix-diff

              (
                pkgs.ollama.override {
                  acceleration = "cuda";
                }
              )
            ];

            process = {
              implementation = "hivemind";
            };

            processes = {
              "ollama" = {
                exec = "ollama serve";
              };
            };
          }
        ];
      };
    };
  };
}
