{self, ...}: {
  perSystem = {system, ...}: {
    packages = {
      "devenv-up" = self.devShells.${system}."default".config.procfileScript;
    };
  };
}
