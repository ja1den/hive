/**
 * @type {import("prettier").Config}
 */
const config = {
  plugins: ["@prettier/plugin-xml"],
  xmlSortAttributesByKey: true,
  xmlWhitespaceSensitivity: "ignore",
};

export default config;
